import React from "react";

function Footer() {
  const year = new Date().getFullYear();
  return (
    <footer>
      <p>ⓒ {year} Even Arefaine</p>
    </footer>
  );
}

export default Footer;
